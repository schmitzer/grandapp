package yauluanenkov.newgrandapp;


import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SpecialAdapter extends ArrayAdapter<NavItem> {

    public SpecialAdapter(Context context, NavItem[] arr) {
        super(context, R.layout.nav_item, arr);
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.nav_item, null);
        }
        final NavItem lesson = getItem(position);
        assert lesson != null;
        ((TextView) convertView.findViewById(R.id.textView34)).setText(lesson.name);
        ((ImageView) convertView.findViewById(R.id.imageView67)).setImageResource(lesson.image);
        convertView.setBackgroundColor(Color.parseColor(lesson.color));
        return convertView;
    }

}