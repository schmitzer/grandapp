package yauluanenkov.newgrandapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class GesturesLesson extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestures_lesson);

        final Button button = findViewById(R.id.gesturesButton);
        final ImageView burger= findViewById(R.id.imageView3);
        button.setOnClickListener(this);
        burger.setOnClickListener(this);
    }
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView3:
                Intent intent = new Intent(this,
                        NavMenu.class);
                startActivity(intent);
                break;
            case R.id.gesturesButton:
                Intent intent2 = new Intent(this,
                        PanelMain.class);
                startActivity(intent2);
                break;
        }
    }
}
