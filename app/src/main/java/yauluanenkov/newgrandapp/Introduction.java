package yauluanenkov.newgrandapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

public class Introduction extends AppCompatActivity implements View.OnClickListener{
    private ImageView firstLesson;
    private VideoView firstVideo;
    private MediaController mediaController;
    private int position = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);

        final Button button = findViewById(R.id.button);
        final ImageView burger= findViewById(R.id.imageView68);
        firstLesson = findViewById(R.id.imageView);
        button.setOnClickListener(this);
        burger.setOnClickListener(this);
        firstLesson.setOnClickListener(this);

        firstVideo = (VideoView) findViewById(R.id.videoView);
        firstVideo.setVisibility(View.GONE);

        if (mediaController == null) {
            mediaController = new MediaController(this);
            mediaController.setAnchorView(firstVideo);
            firstVideo.setMediaController(mediaController);
        }

        firstVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {


                firstVideo.seekTo(position);
                if (position == 0) {
                    firstVideo.start();
                }

                // When video Screen change size.
                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

                        // Re-Set the videoView that acts as the anchor for the MediaController
                        mediaController.setAnchorView(firstVideo);
                    }
                });
            }
        });
    }
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView:
                firstLesson.setVisibility(View.GONE);
                firstVideo.setVisibility(View.VISIBLE);
                int id = this.getRawResIdByName("video");
                firstVideo.setVideoURI(Uri.parse("android.resource://" + getPackageName() +"/"+id));
                firstVideo.requestFocus();
                break;
            case R.id.imageView68:
                Intent intent = new Intent(this,
                        NavMenu.class);
                startActivity(intent);
                break;
            case R.id.button:
                Intent intent2 = new Intent(this,
                        GesturesMain.class);
                startActivity(intent2);
                break;
        }
    }
    public int getRawResIdByName(String resName) {
        // return 0 if not found
        String pkgName = this.getPackageName();
        return this.getResources().getIdentifier(resName, "raw", pkgName);
    }
}
