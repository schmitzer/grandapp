package yauluanenkov.newgrandapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Start extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        final Button gotoFirstLesson = findViewById(R.id.gotoFirstLesson);
        final ImageView burger= findViewById(R.id.burgerMenu);
        gotoFirstLesson.setOnClickListener(this);
        burger.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
       switch (v.getId()) {
        case R.id.burgerMenu:  Intent intent = new Intent(this,
                NavMenu.class);
            startActivity(intent); break;
        case R.id.gotoFirstLesson: Intent intent2 = new Intent(this,
                Introduction.class);
            startActivity(intent2); break;
    }

    }
}
