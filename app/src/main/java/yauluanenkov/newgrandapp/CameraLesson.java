package yauluanenkov.newgrandapp;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class CameraLesson extends AppCompatActivity implements View.OnClickListener{
    int CAMERA_RESULT = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_lesson);

        final Button button = findViewById(R.id.button11);
        final ImageView burger= findViewById(R.id.imageView26);
        final Button photo = findViewById(R.id.button10);
        photo.setOnClickListener(this);
        button.setOnClickListener(this);
        burger.setOnClickListener(this);
    }
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button10:
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_RESULT);
                break;
            case R.id.imageView26:
                Intent intent2 = new Intent(this,
                        NavMenu.class);
                startActivity(intent2);
                break;
            case R.id.button11:
                Intent intent3 = new Intent(this,
                        InternetMain.class);
                startActivity(intent3);
                break;
        }
    }
}
