package yauluanenkov.newgrandapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class TelephoneLesson extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telephone_lesson);

        final Button button = findViewById(R.id.button4);
        final Button button2 = findViewById(R.id.button8);
        final Button button3 = findViewById(R.id.button9);
        final ImageView burger= findViewById(R.id.imageView16);
        button.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        burger.setOnClickListener(this);
    }
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button8:
            case R.id.button9:
                Uri address = Uri.parse("http://myitschool.ru");
                Intent openlinkIntent = new Intent(Intent.ACTION_VIEW, address);
                startActivity(openlinkIntent);
                break;
            case R.id.imageView16:
                Intent intent = new Intent(this,
                        NavMenu.class);
                startActivity(intent);
                break;
            case R.id.button4:
                Intent intent2 = new Intent(this,
                        DesktopMain.class);
                startActivity(intent2);
                break;
        }
    }
}

