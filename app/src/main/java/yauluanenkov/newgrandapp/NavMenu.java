package yauluanenkov.newgrandapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import static android.support.v4.content.ContextCompat.startActivity;

public class NavMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_nav_menu);
        SpecialAdapter adapter = new SpecialAdapter(this, makeLesson());
        ListView lv = (ListView) findViewById(R.id.ListView);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new OnItemClickListener() {
        public void onItemClick(AdapterView<?> parent, View v, int position, long id)
        {
           switch (position) {
               case 0: startActivity(new Intent(NavMenu.this, GesturesMain.class)); break;
               case 1: startActivity(new Intent(NavMenu.this, PanelMain.class));break;
               case 2: startActivity(new Intent(NavMenu.this, KeyboardMain.class)); break;
               case 3: startActivity(new Intent(NavMenu.this, PhoneMain.class)); break;
               case 4: startActivity(new Intent(NavMenu.this, DesktopMain.class)); break;
               case 5: startActivity(new Intent(NavMenu.this, CameraMain.class)); break;
               case 6: startActivity(new Intent(NavMenu.this, InternetMain.class)); break;
               case 7: startActivity(new Intent(NavMenu.this, ShopMain.class)); break;
               case 8: startActivity(new Intent(NavMenu.this, MessengerMain.class)); break;
               case 9: startActivity(new Intent(NavMenu.this, NavigatorMain.class)); break;
               case 10: startActivity(new Intent(NavMenu.this, SettingsLesson.class)); break;

           }
        }
    });


    }

    // Метод cоздания массива
    NavItem[] makeLesson() {
        NavItem[] arr = new NavItem[11];

        String[] namesArr = {"УРОК 1 - ЖЕСТЫ", "УРОК 2 - ПАНЕЛЬ", "УРОК 3 - КЛАВИАТУРА",
                "УРОК 4 - ТЕЛЕФОН И SMS", "УРОК 5 - РАБОЧИЙ СТОЛ", "УРОК 6 - КАМЕРА", "УРОК 7 - ИНТЕРНЕТ",
                "УРОК 8 - GOOGLE PLAY", "УРОК 9 - МЕССЕНДЖЕР", "УРОК 10 - НАВИГАТОР", "УРОК 11 - НАСТРОЙКИ"};

        String[] colorArr = {"#E040FB", "#9575CD", "#7C4DFF", "#2196F3", "#009688", "#43A047", "#FF8F00",
                "#EF5350", "#FF4081", "#F06292", "#757575"};

        int[] dayArr = {R.drawable.gestures, R.drawable.panel, R.drawable.keyboard, R.drawable.sms,
                R.drawable.desktop, R.drawable.camera, R.drawable.internet, R.drawable.market, R.drawable.messenger,
                R.drawable.maps, R.drawable.tumbler};

// Сборка
        for (int i = 0; i < arr.length; i++) {
            NavItem lesson = new NavItem();
            lesson.name = namesArr[i];
            lesson.color = colorArr[i];
            lesson.image = dayArr[i];
            arr[i] = lesson;
        }
        return arr;

    }



}
